import logging
import string
logging.basicConfig(level=logging.INFO)


def parse(router_info_path):
    # data of interest
    ssu_ipv4 = {b'cost=': None, b'caps=': None, b'host=': None,
                b'key=': None, b'port=': None,
                b'iexp0=': None, b'iexp1=': None, b'iexp2=': None,
                b'ihost0=': None, b'ihost1=': None, b'ihost2=': None,
                b'ikey0=': None, b'ikey1=': None, b'ikey2=': None,
                b'iport0=': None, b'iport1=': None, b'iport2=': None,
                b'itag0=': None, b'itag1=': None, b'itag2=': None}
    ssu_ipv6 = {b'cost=': None, b'caps=': None, b'host=': None,
                b'key=': None, b'port=': None}

    ntcp_ipv4 = {b'cost=': None, b'host=': None, b'port=': None,
                 b's=': None, b'i=': None, b'v=': None}
    ntcp_ipv6 = {b'cost=': None, b'host=': None, b'port=': None,
                 b's=': None, b'i=': None, b'v=': None}

    ntcp2_ipv4 = {b'cost=': None, b'host=': None, b'port=': None,
                  b's=': None, b'i=': None, b'v=': None}
    ntcp2_ipv6 = {b'cost=': None, b'host=': None, b'port=': None,
                  b's=': None, b'i=': None, b'v=': None}

    options = {b'caps=': None, b'netId=': None, b'router.version=': None,
               b'netdb.knownLeaseSets=': None, b'netdb.knownRouters=': None,
               b'family=': None, b'family.key=': None, b'family.sig=': None}

    def ip_version_detector(router_address_bytes):
        try:
            ip = router_address_bytes.split(b'host=')[1].split(b';')[0]
            if ip.count(b'.') == 3:
                return 'ipv4'
            elif b':' in ip:
                return 'ipv6'
        except IndexError:
            return None

    def key_value_mapper(router_address_dict, mapping_bytes):
        for k in router_address_dict:
            try:
                value = mapping_bytes.split(k)[1].split(b';')[0]
                router_address_dict[k] = \
                    ''.join([x for x in value[1:].decode()  # 1st byte is len
                             if x in string.printable]).strip()
            except IndexError:
                logging.getLogger(__name__).debug(
                    "{0} not found in {1}".format(k, router_address_dict))
        return router_address_dict

    def router_address_parser(router_address_bytes, mapping_length):
        nonlocal ssu_ipv4
        nonlocal ssu_ipv6
        nonlocal ntcp_ipv4
        nonlocal ntcp_ipv6
        nonlocal ntcp2_ipv4
        nonlocal ntcp2_ipv6

        if router_address_bytes[9:13] == b'\x03SSU':

            logging.getLogger(__name__).debug(
                "Parsing SSU: {}".format(router_address_bytes))

            if b'ihost0=' in router_address_bytes:
                ssu_ipv4[b'cost='] = router_address_bytes[0]
                ssu_ipv4 = key_value_mapper(
                    ssu_ipv4, router_address_bytes[-mapping_length:])
            else:
                if ip_version_detector(router_address_bytes) == 'ipv4':
                    ssu_ipv4[b'cost='] = router_address_bytes[0]
                    ssu_ipv4 = key_value_mapper(
                        ssu_ipv4, router_address_bytes[-mapping_length:])
                elif ip_version_detector(router_address_bytes) == 'ipv6':
                    ssu_ipv6[b'cost='] = router_address_bytes[0]
                    ssu_ipv6 = key_value_mapper(
                        ssu_ipv6, router_address_bytes[-mapping_length:])
                else:
                    return 'malformed SSU!'

        elif router_address_bytes[9:14] == b'\x04NTCP':
            if ip_version_detector(router_address_bytes) == 'ipv4':
                ntcp_ipv4[b'cost='] = router_address_bytes[0]
                key_value_mapper(ntcp_ipv4,
                                 router_address_bytes[-mapping_length:])
            elif ip_version_detector(router_address_bytes) == 'ipv6':
                ntcp_ipv6[b'cost='] = router_address_bytes[0]
                key_value_mapper(ntcp_ipv6,
                                 router_address_bytes[-mapping_length:])
            else:
                return 'malformed NTCP!'

        elif router_address_bytes[9:15] == b'\x05NTCP2':

            if ip_version_detector(router_address_bytes) == 'ipv4':
                ntcp2_ipv4[b'cost='] = router_address_bytes[0]
                key_value_mapper(ntcp2_ipv4,
                                 router_address_bytes[-mapping_length:])
            elif ip_version_detector(router_address_bytes) == 'ipv6':
                ntcp2_ipv6[b'cost='] = router_address_bytes[0]
                key_value_mapper(ntcp2_ipv6,
                                 router_address_bytes[-mapping_length:])
            # case of unpublished NTCP2 address
            # https://geti2p.net/spec/ntcp2#unpublished-ntcp2-address
            elif b'\x01s=' in router_address_bytes and \
                    b'\x01v=' in router_address_bytes:
                ntcp2_ipv4[b'cost='] = router_address_bytes[0]
                key_value_mapper(ntcp2_ipv4,
                                 router_address_bytes[-mapping_length:])
            else:
                return 'malformed NTCP2!'

        else:
            logging.getLogger(__name__).debug(
                "No valid router address field found")

    logging.getLogger(__name__).debug(
        "Parsing routerInfo file: {}".format(router_info_path))

    # Start parsing here

    with open(router_info_path, 'rb') as router_info_file:
        router_info_bytes = router_info_file.read()

    host_router_ip = router_info_path.split("/")[-1].split("_")[1]

    # RouterIdentity
    # https://geti2p.net/spec/common-structures#struct-routeridentity

    # keys
    # https://geti2p.net/spec/common-structures#keysandcert

    byte_pointer = 0
    router_identity_bytes = router_info_bytes[byte_pointer:387]

    certificate_length_bytes = router_info_bytes[385:387]

    certificate_length = int.from_bytes(certificate_length_bytes,
                                        byteorder='big')

    logging.getLogger(__name__).debug(
        "router_identity_bytes: {}".format(router_identity_bytes))
    logging.getLogger(__name__).debug(
        "certificate_length_bytes: {}".format(certificate_length_bytes))
    logging.getLogger(__name__).debug(
        "certificate_length: {}".format(certificate_length))

    # cert
    # https://geti2p.net/spec/common-structures#keysandcert
    byte_pointer = byte_pointer + len(router_identity_bytes)

    certificate_bytes = b''
    if certificate_length > 0:
        certificate_bytes = \
            router_info_bytes[byte_pointer:
                              byte_pointer + certificate_length]
    logging.getLogger(__name__).debug(
        "certificate_bytes: {}".format(certificate_bytes))

    # Date
    # https://geti2p.net/spec/common-structures#type-date
    byte_pointer = byte_pointer + len(certificate_bytes)

    published_date_bytes = \
        router_info_bytes[byte_pointer:byte_pointer + 8]

    published_date = int.from_bytes(published_date_bytes,
                                    byteorder='big') * 0.001  # millisecond

    logging.getLogger(__name__).debug(
        "published_date: {}".format(published_date))

    # The number of RouterAddresses to follow
    # https://geti2p.net/spec/common-structures#type-integer
    byte_pointer = byte_pointer + len(published_date_bytes)

    number_router_addresses_bytes = \
        router_info_bytes[byte_pointer:byte_pointer + 1]

    number_router_addresses = \
        int.from_bytes(number_router_addresses_bytes, byteorder='big')

    logging.getLogger(__name__).debug(
        "number_router_addresses_bytes: {}".format(
            number_router_addresses_bytes))
    logging.getLogger(__name__).debug(
        "number_router_addresses: {}".format(number_router_addresses))

    # Parse RouterAddress fields
    # https://geti2p.net/spec/common-structures#routeraddress
    byte_pointer = byte_pointer + 1
    i = 0
    while i < number_router_addresses:
        transport_style_length = router_info_bytes[byte_pointer + 1 + 8]

        router_address_header_length = 1 + 8 + 1 + transport_style_length
        # cost(1B) + date(8B) + size(1B)

        router_address_mapping_options_length_bytes = \
            router_info_bytes[byte_pointer + router_address_header_length:
                              (byte_pointer + router_address_header_length
                               + 2)]

        router_address_mapping_options_length = int.from_bytes(
            router_address_mapping_options_length_bytes, byteorder='big')

        logging.getLogger(__name__).debug(
            "transport_style_length: {}".format(transport_style_length))
        logging.getLogger(__name__).debug(
            "router_address_header_length: {}".format(
                router_address_header_length))
        logging.getLogger(__name__).debug(
            "router_address_mapping_options_length: {}".format(
                router_address_mapping_options_length))

        router_address_bytes_length = \
            router_address_header_length + 2 + \
            router_address_mapping_options_length

        router_address_parser(
            router_info_bytes[byte_pointer:
                              byte_pointer + router_address_bytes_length],
            router_address_mapping_options_length)

        logging.getLogger(__name__).debug("{} router_address_bytes:{}".format(
            router_address_bytes_length,
            router_info_bytes[byte_pointer:
                              byte_pointer + router_address_bytes_length]))

        byte_pointer = byte_pointer + router_address_bytes_length
        i += 1
    logging.getLogger(__name__).debug('ssu4: {}'.format(ssu_ipv4))
    logging.getLogger(__name__).debug('ssu6: {}'.format(ssu_ipv6))
    logging.getLogger(__name__).debug('ntcp4: {}'.format(ntcp_ipv4))
    logging.getLogger(__name__).debug('ntcp6: {}'.format(ntcp_ipv6))
    logging.getLogger(__name__).debug('ntcp2_4: {}'.format(ntcp2_ipv4))
    logging.getLogger(__name__).debug('ntcp2_6: {}'.format(ntcp2_ipv6))
    logging.getLogger(__name__).debug('options: {}'.format(options))

    # Parse options
    # https://geti2p.net/spec/common-structures#type-mapping

    # peer size, not used yet
    # peer_size = int.from_bytes(
    #     router_info_bytes[byte_pointer: byte_pointer+1], byteorder='big')

    byte_pointer = byte_pointer + 1

    options_mapping_length_bytes = router_info_bytes[byte_pointer:
                                                     byte_pointer + 2]

    options_mapping_length = int.from_bytes(options_mapping_length_bytes,
                                            byteorder='big')

    byte_pointer = byte_pointer + 2

    key_value_mapper(options,
                     router_info_bytes[byte_pointer:
                                       byte_pointer + options_mapping_length])

    # signature (normally 40 bytes)
    # byte_pointer = byte_pointer + options_mapping_length
    # signature = router_info_bytes[byte_pointer:]  # not used yet
    router_info = (host_router_ip, router_identity_bytes, published_date,
                   number_router_addresses, ssu_ipv4, ssu_ipv6, ntcp_ipv4,
                   ntcp_ipv6, ntcp2_ipv4, ntcp2_ipv6, options)
    return router_info
